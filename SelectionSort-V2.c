//
// Created by Raman
//
#include <stdlib.h>
#include <stdio.h>

int ARRAY_SIZE = 9;

// A helper function to print out content of array
void printArray(int array[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main() {
    //  unsorted array
    int array[] = {12,4,5,7,0,1,50,9,6};

    //  print array
    printf("Unsorted array is: \n");
    printArray(array);

    //loop to sort the array in descending order
    for (int i = 0; i < ARRAY_SIZE; i++) {

        for (int j = 0; j < ARRAY_SIZE-1; j++) {

            if (array[j] < array[j+1]) {

                //swap the numbers
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }

        }

    }

    //  print sorted array
    printf("Sorted array is: \n");
    printArray(array);


    return 0;
}
