#include <stdlib.h>
#include <stdio.h>

// global variable to keep track of array size
int ARRAY_SIZE = 6;

// helper function to print out the array
void printArray(int arr[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n-------\n");
}

int main() {

    // here is the unsorted array
    int array[] = {8,4,1,9,0,2};
    printf("Original array:\n");
    printArray(array);

    //new array to store sorted numbers
    int sortedArray[ARRAY_SIZE] ;

    //loop to sort the array
    for (int i = 0; i < ARRAY_SIZE; i++)
    {

        int largest = array[0];
        int positionOflargest = 0;

        for (int j = 0; j < ARRAY_SIZE; j++)
        {

            if (array[j] > largest) {
                positionOflargest = j;
                largest = array[j];
            }

        }

        sortedArray[i] = largest;

        //fake delete
        array[positionOflargest] = -999999;

    }

    //print the results
    printf("Results after fake delete: \n");
    printArray(array);
    printf("Results after sorting: \n");
    printArray(sortedArray);


    return 0;
}
